Changelog
=========

1.1.0
-----

Main:
- feat: add grub.cfg generation for EFI

Misc:
- chore: include test-cookbook gitlab-ci
- doc: use doc in git message instead of docs
- chore: add 2018 to copyright notice
- chore: set generic maintainer & helpdesk email
- chore: add supermarket category in .category

1.0.0
-----

- Initial version with Centos 7 support, verified for kernel 4.9.62
